package com.madbrains.testapp.utils.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitController {

    private val GITHUB_SERVER_URL = "https://api.github.com/"

    private var githubApi: GithubApi? = null

    private fun getLogging(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

    private fun getOkHttp(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(getLogging())
                .build()
    }

    fun getGithubApi(): GithubApi {
        if (githubApi == null) {
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                    .baseUrl(GITHUB_SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(getOkHttp())
                    .build()

            githubApi = retrofit.create(GithubApi::class.java)
        }
        return githubApi!!
    }

}