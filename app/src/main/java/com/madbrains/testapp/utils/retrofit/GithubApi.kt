package com.madbrains.testapp.utils.retrofit

import com.madbrains.testapp.models.CommitDetails
import com.madbrains.testapp.models.RepositoriesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubApi {

    @GET("/search/repositories")
    fun getRepositories(@Query("q") q: String,
                        @Query("page") page: String,
                        @Query("per_page") per_page: String): Call<RepositoriesResponse>

    @GET("/repos/{owner}/{repo}/commits")
    fun getCommits(@Path(value = "owner", encoded = true) owner: String,
                   @Path(value = "repo", encoded = true) repo: String,
                   @Query("q") q: String,
                   @Query("per_page") per_page: String): Call<List<CommitDetails>>

}