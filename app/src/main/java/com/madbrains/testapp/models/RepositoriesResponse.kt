package com.madbrains.testapp.models

class RepositoriesResponse {

    var total_count: Int? = null

    var incomplete_results: Boolean? = null

    var items: List<Repository>? = null

}