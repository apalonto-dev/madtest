package com.madbrains.testapp.models

import java.io.Serializable

class RepositoryOwner : Serializable {

    var login: String? = null

    var avatar_url: String? = null

}