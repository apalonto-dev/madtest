package com.madbrains.testapp.models

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
class Repository : Serializable {

    @PrimaryKey
    var id: Int? = null

    var name: String? = null

    var description: String? = null

    var language: String? = null

    var forks_count: Int? = null

    var stargazers_count: Int? = null

    @Embedded
    var owner: RepositoryOwner? = null

}