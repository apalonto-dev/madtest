package com.madbrains.testapp.models

class CommitDetails {

    var commit: Commit? = null

    var author: CommitAuthor? = null

}