package com.madbrains.testapp

import android.app.Application
import com.madbrains.testapp.room.AppDatabase
import android.arch.persistence.room.Room

class App : Application() {

    companion object {
        private var instance: App? = null
    }

    private var database: AppDatabase? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
                .allowMainThreadQueries()
                .build()
    }

    fun getInstance(): App? {
        return instance
    }

    fun getDatabase(): AppDatabase? {
        return database
    }

}