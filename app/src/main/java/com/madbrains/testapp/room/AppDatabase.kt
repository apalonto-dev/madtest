package com.madbrains.testapp.room

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import com.madbrains.testapp.models.Repository
import com.madbrains.testapp.room.dao.RepositoryDao

@Database(entities = [Repository::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun repositoryDao(): RepositoryDao

}