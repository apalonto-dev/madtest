package com.madbrains.testapp.room.dao

import android.arch.persistence.room.*
import com.madbrains.testapp.models.Repository

@Dao
interface RepositoryDao {

    @get:Query("SELECT * FROM repository")
    val all: List<Repository>

    @Insert
    fun insert(repository: Repository)

    @Update
    fun update(repository: Repository)

    @Delete
    fun delete(repository: Repository)

}