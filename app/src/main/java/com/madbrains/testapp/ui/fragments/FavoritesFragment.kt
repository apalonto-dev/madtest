package com.madbrains.testapp.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.madbrains.testapp.App
import com.madbrains.testapp.R
import com.madbrains.testapp.models.Repository
import com.madbrains.testapp.ui.adapters.RepositoryAdapter
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment : BaseFragment() {

    private var favorites: MutableList<Repository> = mutableListOf()
    private var repositoryAdapter: RepositoryAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val db = App().getInstance()!!.getDatabase()
        val repositoryDao = db?.repositoryDao()
        favorites.addAll(repositoryDao!!.all)

        val layoutManager = LinearLayoutManager(context!!)
        repositoryAdapter = RepositoryAdapter(favorites, context!!, null)
        favoritesRecyclerView.layoutManager = layoutManager
        favoritesRecyclerView.adapter = repositoryAdapter
        favoritesRecyclerView.setHasFixedSize(true)
        repositoryAdapter?.notifyDataSetChanged()

    }

}