package com.madbrains.testapp.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.madbrains.testapp.R
import com.madbrains.testapp.models.CommitDetails
import com.madbrains.testapp.models.Repository
import com.madbrains.testapp.ui.adapters.CommitAdapter
import com.madbrains.testapp.utils.retrofit.GithubApi
import com.madbrains.testapp.utils.retrofit.RetrofitController
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.activity_repository_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryDetailsActivity : AppCompatActivity() {

    private val perPage: Int = 10

    private var commits: MutableList<CommitDetails> = mutableListOf()
    private var githubApi: GithubApi? = null
    private var commitAdapter: CommitAdapter? = null
    private var repository: Repository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val layoutManager = LinearLayoutManager(this)
        commitAdapter = CommitAdapter(commits, this)
        commitRecyclerView.layoutManager = layoutManager
        commitRecyclerView.adapter = commitAdapter
        commitRecyclerView.setHasFixedSize(true)

        githubApi = RetrofitController.getGithubApi()

        getDataFromBundle()

        showRepositoryDetails()

        getCommits()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    private fun getDataFromBundle() {
        val intent = this.intent
        val bundle = intent.extras
        repository = bundle!!.getSerializable("repository") as Repository
    }

    private fun getCommits() {
        showProgressBar()
        githubApi!!.getCommits(repository?.owner?.login.toString(), repository?.name.toString(), "test", perPage.toString())
                .enqueue(getCommitsCallback)
    }

    private fun showProgressBar() {
        this.runOnUiThread { progressBar.visibility = View.VISIBLE }
    }

    private fun hideProgressBar() {
        this.runOnUiThread { progressBar.visibility = View.GONE }
    }

    private fun showRepositoryDetails() {
        repositoryNameTextView.text = repository?.name
        ImageLoader.getInstance().displayImage(repository?.owner?.avatar_url, ownerAvatarImageView)
        ownerNameTextView.text = repository?.owner?.login
    }

    private val getCommitsCallback = object : Callback<List<CommitDetails>> {
        override fun onResponse(call: Call<List<CommitDetails>>?, response: Response<List<CommitDetails>>?) {
            hideProgressBar()
            commits.addAll(response?.body()!!)
            commitAdapter?.notifyDataSetChanged()
        }

        override fun onFailure(call: Call<List<CommitDetails>>?, t: Throwable?) {
            hideProgressBar()
        }

    }

}
