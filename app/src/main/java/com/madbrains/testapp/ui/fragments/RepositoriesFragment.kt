package com.madbrains.testapp.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.madbrains.testapp.R
import com.madbrains.testapp.models.RepositoriesResponse
import com.madbrains.testapp.models.Repository
import com.madbrains.testapp.ui.adapters.RepositoryAdapter
import com.madbrains.testapp.utils.retrofit.GithubApi
import com.madbrains.testapp.utils.retrofit.RetrofitController
import kotlinx.android.synthetic.main.fragment_repositories.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.madbrains.testapp.App
import com.madbrains.testapp.room.dao.RepositoryDao
import com.madbrains.testapp.ui.activities.MainActivity

class RepositoriesFragment : BaseFragment() {

    private val perPage: Int = 10

    private var repositoryDao: RepositoryDao? = null
    private var repositories: MutableList<Repository> = mutableListOf()
    private var githubApi: GithubApi? = null
    private var repositoryAdapter: RepositoryAdapter? = null
    private var page: Int = 1
    private var lastPage: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repositories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val db = App().getInstance()!!.getDatabase()
        repositoryDao = db?.repositoryDao()

        val layoutManager = LinearLayoutManager(context!!)
        repositoryAdapter = RepositoryAdapter(repositories, context!!, object : RepositoryAdapter.ClickListener {
            override fun onClick(position: Int) {
                val repository: Repository = repositories[position]
                (activity as MainActivity).toRepositoryDetails(repository)
            }
        })
        repositoryRecyclerView.layoutManager = layoutManager
        repositoryRecyclerView.adapter = repositoryAdapter
        repositoryRecyclerView.setHasFixedSize(true)

        githubApi = RetrofitController.getGithubApi()

        getRepositories()

        repositorySwipeLayout.setOnRefreshListener { refreshContent() }

    }

    fun saveRepository(repository: Repository) {
        repositoryDao?.insert(repository)
    }

    private fun refreshContent() {
        page = 1
        lastPage = false
        repositories.clear()
        repositoryAdapter?.notifyDataSetChanged()
        getRepositories()
    }

    private fun getRepositories() {
        if (lastPage) {
            return
        }
        if (!repositorySwipeLayout.isRefreshing) {
            showProgressBar()
        }
        githubApi!!.getRepositories("test", page.toString(), perPage.toString()).enqueue(getRepositoriesCallback)

    }

    private val getRepositoriesCallback = object : Callback<RepositoriesResponse> {
        override fun onResponse(call: Call<RepositoriesResponse>?, response: Response<RepositoriesResponse>?) {
            if (activity == null) {
                return
            }
            hideProgressBar()
            repositorySwipeLayout.isRefreshing = false
            val repositoriesList = response?.body()?.items
            if (repositoriesList != null && repositoriesList.size == perPage) {
                repositories.addAll(repositoriesList)
            } else {
                lastPage = true
            }
            repositoryAdapter?.notifyDataSetChanged()
        }

        override fun onFailure(call: Call<RepositoriesResponse>?, t: Throwable?) {
            if (activity == null) {
                return
            }
            hideProgressBar()
            repositorySwipeLayout.isRefreshing = false
        }

    }

}