package com.madbrains.testapp.ui.fragments

import android.support.v4.app.Fragment
import com.madbrains.testapp.ui.activities.MainActivity

abstract class BaseFragment : Fragment() {

    fun showProgressBar() {
        if (activity != null) {
            (activity as MainActivity).showProgressBar()
        }
    }

    fun hideProgressBar() {
        if (activity != null) {
            (activity as MainActivity).hideProgressBar()
        }
    }

    override fun onDestroy() {
        hideProgressBar()
        super.onDestroy()
    }

}