package com.madbrains.testapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.madbrains.testapp.R
import com.madbrains.testapp.models.Repository
import com.madbrains.testapp.ui.fragments.FavoritesFragment
import com.madbrains.testapp.ui.fragments.RepositoriesFragment
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val FRAGMENT_REPOSITORIES = "fragmentRepositories"
    private val FRAGMENT_FAVORITES = "fragmentFavorites"

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_repositories -> {
                replaceToRepositoriesFragment()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {
                replaceToFavoritesFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        initImageLoader()

        replaceToRepositoriesFragment()

    }

    private fun initImageLoader() {
        val options = DisplayImageOptions.Builder()
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnLoading(R.drawable.placeholder_photo)
                .cacheOnDisk(true)
                .build()

        val config = ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .build()

        ImageLoader.getInstance().init(config)
    }

    fun showProgressBar() {
        this.runOnUiThread { progressBar.visibility = View.VISIBLE }
    }

    fun hideProgressBar() {
        this.runOnUiThread { progressBar.visibility = View.GONE }
    }

    private fun replaceToRepositoriesFragment() {
        val repositoriesFragment = RepositoriesFragment()
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, repositoriesFragment, FRAGMENT_REPOSITORIES)
        ft.commit()
    }

    private fun replaceToFavoritesFragment() {
        val favoritesFragment = FavoritesFragment()
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.container, favoritesFragment, FRAGMENT_FAVORITES)
        ft.commit()
    }

    fun toRepositoryDetails(repository: Repository) {
        val intent = Intent(this, RepositoryDetailsActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable("repository", repository)
        intent.putExtras(bundle)
        startActivity(intent)
    }

}
