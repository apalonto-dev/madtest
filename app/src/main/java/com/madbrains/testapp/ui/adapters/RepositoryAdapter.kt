package com.madbrains.testapp.ui.adapters

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.madbrains.testapp.R
import com.madbrains.testapp.models.Repository
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryAdapter(private val repositories: List<Repository>, val context: Context, private val clickListener: ClickListener?) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_repository, parent, false))
    }

    companion object {
        var mClickListener: ClickListener? = null
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository : Repository = repositories[position]
        mClickListener = clickListener

        holder.tvName.text = repository.name.toString()
        holder.tvLanguage.text = repository.language.toString()
        holder.tvDescription.text = repository.description.toString()
        holder.tvStarsCount.text = repository.stargazers_count.toString()
        holder.tvForksCount.text = repository.forks_count.toString()

        holder.contentLayout.setOnClickListener {
            if (mClickListener != null)
                mClickListener?.onClick(position)
        }

    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    interface ClickListener {
        fun onClick(position: Int)
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val contentLayout: ConstraintLayout = view.repositoryContentLayout
        val tvName: TextView = view.nameTextView
        val tvLanguage: TextView = view.languageTextView
        val tvDescription: TextView = view.descriptionTextView
        val tvStarsCount: TextView = view.starsCountTextView
        val tvForksCount: TextView = view.forksCountTextView
    }

}