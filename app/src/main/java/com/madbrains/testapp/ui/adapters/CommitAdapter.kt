package com.madbrains.testapp.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.madbrains.testapp.R
import com.madbrains.testapp.models.CommitDetails
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.item_commit.view.*

class CommitAdapter(private val commits: List<CommitDetails>, val context: Context) : RecyclerView.Adapter<CommitAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_commit, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val commit : CommitDetails = commits[position]

        holder.tvCommitDescription.text = commit.commit?.message.toString()
        holder.tvUserName.text = commit.author?.login.toString()
        ImageLoader.getInstance().displayImage(commit.author?.avatar_url, holder.ivUserAvatar)

    }

    override fun getItemCount(): Int {
        return commits.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val tvCommitDescription: TextView = view.commitDescriptionTextView
        val tvUserName: TextView = view.userNameTextView
        val ivUserAvatar: ImageView = view.userAvatarImageView
    }

}